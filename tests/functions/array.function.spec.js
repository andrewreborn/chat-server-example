// Core
const expect = require('chai').expect;

// Functions
const arrayUtils = require('../../functions/array.function');

describe('Test array functions', () => {
    it('Flatten an array that doesn\'t need to be flattened', () => {
        const arr = [1, 2, 3, 4, 5];
        const flt = arrayUtils.flatten(arr);

        expect(arr.length).to.equal(flt.length);
    });

    it('Flatten a square matrix', () => {
        const arr = [[1, 2], [3, 4], [5, 6]];
        const flt = arrayUtils.flatten(arr);

        expect(flt.length).to.equal(6);
    });

    it('Flatten a more complex matrix', () => {
        const arr = [
            [1, 2, [
                1, 2, 3, 4, [
                    1, 2
                ], 5, 6]
            ], [3,
                [1, 2, 3, 4, 5,
                    [1,
                        [1, 2, 3,
                            [1, 2,
                                [1, 2]
                            ]
                        ], 3
                    ]
                ], 4
            ], [5, 6]
        ];
        const flt = arrayUtils.flatten(arr);

        expect(flt.length).to.equal(28);
    });
});