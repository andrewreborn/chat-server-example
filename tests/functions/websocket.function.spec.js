const expect = require('chai').expect;
const mockSocket = require('mock-socket');

// Classes
const Client = require('../../classes/client').default;

// Functions
const websocketUtils = require('../../functions/websocket.function');

// Other constants
const FAKE_URL = 'ws://localhost:12345';

describe('Testing WebSocket custom functions', () => {
    /**
     * An array of mocked clients
     */
    let clients;

    /**
     * A mocked server
     */
    let server;

    /**
     * A mocked WebSocket to pass to a client
     */
    let ws;

    before(() => {
        server = new mockSocket.Server(FAKE_URL);

        // The websocket to find in the stack
        ws = new mockSocket.WebSocket(FAKE_URL);
        
        clients = [
            new Client(ws, 'cl1'),
            new Client(new mockSocket.WebSocket(FAKE_URL), 'cl2'),
            new Client(new mockSocket.WebSocket(FAKE_URL), 'cl3')
        ];
    });

    it('getNameByClient Fn', () => {
        expect( websocketUtils.getNameByClient(clients, ws) ).to.equal('cl1');
    });

    it('removeClient with a ws that exists', () => {
        expect( websocketUtils.removeClient(clients, ws) ).to.equal(true);
        expect(clients.length).to.be.equal(2);
    });

    it('removeClient with a ws that no longer exists', () => {
        expect( websocketUtils.removeClient(clients, ws) ).to.equal(false);
        expect(clients.length).to.be.equal(2);
    });
});