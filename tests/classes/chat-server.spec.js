// Core
const WebSocket = require('ws');
const expect = require('chai').expect;

// Classes
const ChatServer = require('../../classes/chat-server').ChatServer;

// Constants
const MESSAGE_FROM_USER_ONE = 'MESSAGE TO USER 2';
const MESSAGE_FROM_USER_TWO = 'MESSAGE TO USER 1';

describe('Testing chat server functionalities', () => {
    /**
     * A variable that will be set in the "before" hook
     */
    let server;

    /**
     * Holds a connection for a user called "user1"
     */
    let userOne;

    /**
     * Holds a connection for a user called "user2"
     */
    let userTwo;

    after(() => {
        userOne.close();
        userTwo.close();
        server.close();
    });

    before(() => {
        server = new ChatServer();
        userOne = new WebSocket('ws://localhost:10000/?name=user1');
        userTwo = new WebSocket('ws://localhost:10000/?name=user2');
    });
    
    it('WebSocket connection on userOne', done => {
        userOne.on('open', () => {
            done();
        });
    });

    it('WebSocket connected clients: verify that the connection is still alive', () => {
        expect(server.connected).to.be.equal(2);
    });

    it('WebSocket connection (user1) sends a message to user2', done => {
        userTwo.on('message', msg => {
            expect(msg).to.be.equal(`user1 says: ${MESSAGE_FROM_USER_ONE}`);
            done();
        });

        userOne.send(MESSAGE_FROM_USER_ONE);
    });

    it('WebSocket connection (user2) sends a message to user1', done => {
        userOne.on('message', msg => {
            expect(msg).to.be.equal(`user2 says: ${MESSAGE_FROM_USER_TWO}`);
            done();
        });

        userTwo.send(MESSAGE_FROM_USER_TWO);
    });
});