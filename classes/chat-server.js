// Core dependencies
const express = require('express');
const expressWs = require('express-ws');

// Class declarations
const Client = require('./client').default;

// Functions
const websocketUtils = require('../functions/websocket.function');

/**
 * Creates a Chat Server that listens for clients and broadcasts messages
 * @param {number} port The port to listen for incoming connections
 */
class ChatServer {
    constructor(port = 10000) {
        this._clients = [];
        this._expressWs = expressWs(express());
        this._setup(port);
    }

    /**
     * Returns the express app
     * @returns {*}
     */
    get app() {
        return this._expressWs.app;
    }

    /**
     * Number of clients currently connected
     * @returns {number}
     */
    get connected() {
        return this._clients.length;
    }

    /**
     * Getting the underlying websocket, useful for iterating against the connected clients
     * @returns {*}
     */
    get wss() {
        return this._expressWs.getWss();
    }

    /**
     * A callback executed when the close event occours on the Connection
     * @param {WebSocket} ws 
     */
    _onClose(ws) {
        console.log(`${websocketUtils.getNameByClient(this._clients, ws)} left the party ):`);
        websocketUtils.removeClient(this._clients, ws);
    }

    _onConnection(ws, req) {
        const name = req.query.name ? req.query.name : 'anonymous';
        this._clients.push(new Client(ws, name));
    
        console.log(`${name} has joined the party (:`);
    }

    /**
     * Broadcast the message to the listening clients, excluding the current ws
     * 
     * @param {string} msg 
     * @param {WebSocket} ws 
     */
    _onMessage(msg, ws) {
        websocketUtils.broadcast(
            this._clients,
            `${websocketUtils.getNameByClient(this._clients, ws)} says: ${msg}`,
            ws
        );
    }

    /**
     * Initialize the chat server and hook to the server's events
     * 
     * @param {number} port The port to listen for incoming connections
     */
    _setup(port) {
        this._expressWs.getWss().on('connection', (ws, req) => this._onConnection(ws, req));

        this.app.ws('/', (ws, res) => {
            ws.on('close', () => this._onClose(ws));
            ws.on('error', err => console.error(err));
            ws.on('message', msg => this._onMessage(msg, ws));
        });

        this.app.listen(port);
    }

    /**
     * Close the WebSocket
     */
    close() {
        this.wss.close();
    }
}

module.exports = {
    ChatServer
}