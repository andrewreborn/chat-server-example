'use strict';

class Client {
    constructor(websocket, name) {
        this.name = name;
        this.websocket = websocket;
    }
}

module.exports.default = Client;