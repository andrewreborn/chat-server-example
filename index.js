'use strict';

// Imports
const ChatServer = require('./classes/chat-server').ChatServer;

// Run the server
const server = new ChatServer();