'use strict';

const WebSocket = require('ws');

/**
 * Broadcast the message to the given clients
 * 
 * @param {Client[]} clients Clients that will receive the message
 * @param {string} message The message to broadcast
 * @param {WebSocket} source The WebSocket that sent the message, will be excluded from the transmission
 */
const broadcast = (clients = [], message, source) => {
    const broadcastTo = (source instanceof WebSocket)
        ? clients.filter(client => client.websocket !== source)
        : clients;
    
    broadcastTo.forEach(c => c.websocket.send(message));
};

/**
 * Finds a client by the given ws and returns the client's name
 * 
 * @param {Client[]} clients 
 * @param {WebSocket} ws 
 */
const getNameByClient = (clients, ws) => {
    const client = clients.find(client => client.websocket === ws);
    return (client) ? client.name : '';
};

/**
 * Remove the given ws from the clients stack
 * The array is edited by reference!!
 * 
 * @param {Client[]} clients The clients stack
 * @param {WebSocket} ws WebSocket to remove
 * @returns {boolean} True if the ws has been removed, false otherwise
 */
const removeClient = (clients, ws) => {
    const pos = clients.findIndex(c => c.websocket === ws);
    
    if (pos === -1) {
        return false;
    }

    clients.splice(pos, 1);
    return true;
};

module.exports = {
    broadcast,
    getNameByClient,
    removeClient
};