const flatten = (matrix) => {
    const ret = [];

    for (const a of matrix) {
        (a instanceof Array) ? ret.push( ...flatten(a) ) : ret.push(a);
    }

    return ret;
};

module.exports = {
    flatten
};