# Readme

A simple chat system that broadcast messages to the connected clients


## Setup

The following steps are required for running the application:

- Install the required dependencies: from the terminal, type **npm install**
- Run the application with **npm run start**

Another npm script is available for developing purposes: running **npm run develop**, the server will be fired through nodemon, enabling hot reload. Remember that using nodemon, the clients will be disconnected every time the script will be reloaded.


## Using the server with Telnet

A telnet-like application has been provided as a dependency to the project for easing the test of the application. It's available through the **npm run connect** script and should be used as follows:

    npm run connect <address>:<port>?name=<your name>
    
for example:

    npm run connect localhost:10000?name=andrew

will start a connection to the chat server and will send messages with the *andrew* username

## Testing the application
The application has an almost-100% coverage. Tests are provided with chai and mocha: for running the tests suite, use the following command:

    npm run test

- As the chat server class uses WebSockets, only integration tests are provided
- WebSockets utility functions have been unit tested, mocking the sockets and the server

## Notes
The application **does not provide** any security countermeasure against xss or injection attacks as the messages are not stored into a database and are intended to be broadcasted to telnet clients only. The development has strictly followed the assigned task and is not to be considered as a production-ready application (i.e. to be used as a chat on a website).

## Array flattening
The array-flattening task has been solved into this repository. The function has been developed following the TDD paradigm and has been tested against a couple of different scenarios. Tests are executed within the chat server tests.
